﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace CalanceInterviewTest
{
    class Program
    {
        public static string Credentials { get; set; }
        public static string ResponseCode { get; set; }
        public static string Website { get; set; }

        static void Main(string[] args)
        {
            SortedDictionary<DateTime, string> commitMessages = GetCommitMessages();
            WriteMessagesToTextFile(commitMessages);
        }

        //Gets commit messages from BitBucket commits page and
        //populates sorted dictionary by DateTime written
        public static SortedDictionary<DateTime, string> GetCommitMessages()
        {
            SortedDictionary<DateTime, string> commitMessages = new SortedDictionary<DateTime, string>();

            try
            {
                Website = "https://api.bitbucket.org/2.0/repositories/asalvadore/api-test/commits/";
                Credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("asalvadore" + ":" + "USF#1BULLS"));
                object[] Parameters = new object[2] { Credentials, Website };
                GetRequest(Parameters, out HttpWebRequest request, out HttpWebResponse response);

                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    ResponseCode = sr.ReadToEnd();
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Thread.Sleep(10000);
            }

            Thread.Sleep(500);

            //Reads JSON formatted information and creates a dictionary based off of the data

            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> JsonTopObject = (Dictionary<string, object>)jss.DeserializeObject(ResponseCode);
            object[] values = (object[])JsonTopObject["values"];

            //Reads each message set and parses data to pull the 
            //message information and date it was committed

            foreach (object valueInfo in values)
            {
                Dictionary<string, object> valueData = (Dictionary<string, object>)valueInfo;
                Dictionary<string, object> summary = (Dictionary<string, object>)valueData["summary"];
                DateTime time = Convert.ToDateTime(valueData["date"]);
                string msg = Convert.ToString(summary["raw"]).Replace("\n", "");
                commitMessages.Add(time, msg);
            }

            return commitMessages;
        }

        //Writes contents of commitMessages dictionary to text file
        public static void WriteMessagesToTextFile(SortedDictionary<DateTime, string> commitMessages)
        {
            //Creates folder if it does not exist already

            Directory.CreateDirectory("C:\\Anthony_Salvadore_CalanceInterviewTest");

            //Checks to see if there is already a file called CommitMessages
            //in a folder called Anthony_Salvadore_CalanceInterviewTest in the C: drive.
            //If not, will write all contents of commitMessages dictionary.
            //Otherwise, will check what is already in it and write any new messages

            if (!System.IO.File.Exists(@"C:\Anthony_Salvadore_CalanceInterviewTest\CommitMessages.txt"))
            {
                StreamWriter sw = new StreamWriter(@"C:\Anthony_Salvadore_CalanceInterviewTest\CommitMessages.txt", true);

                foreach (string msg in commitMessages.Values)
                {
                    sw.WriteLine(msg);
                }

                sw.Close();
            }
            else
            {
                List<string> writtenMessages = new List<string>();

                StreamReader str = new StreamReader(@"C:\Anthony_Salvadore_CalanceInterviewTest\CommitMessages.txt");
                string line = str.ReadLine();

                while (line != null)
                {
                    writtenMessages.Add(line);
                    line = str.ReadLine();
                }

                str.Close();

                StreamWriter sw = new StreamWriter(@"C:\Anthony_Salvadore_CalanceInterviewTest\CommitMessages.txt", true);

                foreach (string msg in commitMessages.Values)
                {
                    if (!writtenMessages.Contains(msg))
                    {
                        sw.WriteLine(msg);
                    }
                }

                sw.Close();
            }
        }

        //My GET request method, written with info specifically for this project
        public static void GetRequest(object[] Parameters, out HttpWebRequest request, out HttpWebResponse response)
        {
            request = (HttpWebRequest)WebRequest.Create((string)Parameters[1]);
            request.Headers.Add("Authorization", "Basic " + (string)Parameters[0]);
            response = (HttpWebResponse)request.GetResponse();
        }
    }
}